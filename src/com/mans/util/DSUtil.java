package com.mans.util;

import java.util.LinkedList;
import java.util.stream.IntStream;

import com.mans.ds.TreeNode;

public class DSUtil {

	public static TreeNode parseArrayToTree(int[] arr) {
		return parseArrayToTree(arr, 0);
	}

	private static TreeNode parseArrayToTree(int[] arr, int n) {
		if (n > arr.length - 1)
			return null;

		TreeNode current = new TreeNode(arr[n]);

		current.left = parseArrayToTree(arr, (2 * n + 1));
		current.right = parseArrayToTree(arr, (2 * n + 2));

		return current;
	}

	public static TreeNode parseArrayToTree(Integer[] arr) {
		return parseArrayToTree(arr, 0);
	}

	private static TreeNode parseArrayToTree(Integer[] arr, int n) {
		if (n > arr.length - 1 || arr[n] == null)
			return null;

		TreeNode current = new TreeNode(arr[n].intValue());

		current.left = parseArrayToTree(arr, (2 * n + 1));
		current.right = parseArrayToTree(arr, (2 * n + 2));

		return current;
	}

	public static int[] generateNumbersForHeight(int n) {
		int end = (int) Math.pow(2, n) - 1;
		return IntStream.rangeClosed(1, end).toArray();
	}

	public static void printTreeWithNullChildren(TreeNode root) {
		LinkedList<TreeNode> queue = new LinkedList<>();
		queue.offer(root);

		while (!queue.isEmpty()) {
			TreeNode cur = queue.poll();
			String nodeVal = cur == null ? "null" : String.valueOf(cur.val);
			System.out.println(nodeVal);
			if (cur!= null) {
				queue.offer(cur.left);
				queue.offer(cur.right);
			}
		}
	}
}
