package com.mans.ds;

public class ListNode {
	
	private int val;
	private ListNode next;
	
	public ListNode(int data) {
		this.val = data;
		this.next = null;
	}
	
	public ListNode(int data, ListNode next) {
		this.val = data;
		this.next = next;
	}

	public int getData() {
		return val;
	}

	public void setData(int data) {
		this.val = data;
	}

	public ListNode getNext() {
		return next;
	}

	public void setNext(ListNode next) {
		this.next = next;
	}
}
