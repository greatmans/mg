package com.mans.ds;

public class TreeNode {

	public int val;
	public TreeNode left;
	public TreeNode right;

	public TreeNode(int data) {
		this.val = data;
		this.left = null;
		this.right = null;
	}

	public TreeNode(int data, TreeNode left, TreeNode right) {
		this.val = data;
		this.left = left;
		this.right = right;
	}
}
