package com.mans.general.tree;

import com.mans.ds.TreeNode;
import com.mans.util.DSUtil;

public class BinaryTreeIsHead {

	public static void main(String[] args) {
		//TreeNode root = DSUtil.parseArrayToTree(new int []{ 2, 3, 4, 5, 6, 8, 10});
		//TreeNode root = DSUtil.parseArrayToTree(new int []{ 2, 3, 4, 5, 6, 2, 10});
		TreeNode root = createIncompleteBinaryTree();
		if(root == null || checkTreeIsMinHeap(root))
			System.out.println("Tree is min-heap");
		else
			System.out.println("Tree is not min-heap");
	}
	
	public static boolean checkTreeIsMinHeap(TreeNode root) {
		
		return checkTreeIsMinHeap(root, 0, size(root));
	}
	
	private static int size(TreeNode root) {
		if(root == null)
			return 0;
		
		return 1 + size(root.left) + size(root.right);
	}

	private static boolean checkTreeIsMinHeap(TreeNode root, int index, int totalNodes) {
		if(root.left == null && root.right == null) {
			return true;
		}
		
		if(index > totalNodes)
		{
			return false;
		}
		
		if(root.left != null && root.right != null) {
			if(root.val < root.left.val && root.val < root.right.val) {
				boolean lCheck = checkTreeIsMinHeap(root.left, (2*index + 1), totalNodes);
				boolean rCheck = checkTreeIsMinHeap(root.right, (2*index + 2), totalNodes);
				
				if(lCheck && rCheck) {
					return true;
				}
			}
		} else {
			return false;
		}
		
		return false;
	}
	
	private static TreeNode createIncompleteBinaryTree() {
		TreeNode root = new TreeNode(2);
		root.left = new TreeNode(3);
		root.right = new TreeNode(4);
		root.left.left = new TreeNode(4);
		root.right.left = new TreeNode(5);
		root.right.right = new TreeNode(6);
		
		return root;
		
	}

}
