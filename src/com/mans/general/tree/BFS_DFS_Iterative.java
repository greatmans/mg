package com.mans.general.tree;

import java.util.LinkedList;
import java.util.Queue;

import com.mans.ds.TreeNode;
import com.mans.util.DSUtil;

public class BFS_DFS_Iterative {

	public static void main(String[] args) {
		TreeNode root = DSUtil.parseArrayToTree(new int[] { 1, 2, 3, 4, 5, 6, 7 });
		
		BFS_Iterative(root);
		System.out.println("BFS iterative Completed");

		System.out.println("===================================");

		DFS_Iterative(root);
		System.out.println("DFS Pre order iterative Completed");
	}

	public static void BFS_Iterative(TreeNode n) {
		Queue<TreeNode>	 queue = new LinkedList<>();
		queue.offer(n);

		while (!queue.isEmpty()) {
			TreeNode current = queue.poll();
			System.out.println(current.val);

			if (current.left != null)
				queue.offer(current.left);

			if (current.right != null)
				queue.offer(current.right);
		}
	}

	public static void DFS_Iterative(TreeNode n) {
		LinkedList<TreeNode> stack = new LinkedList<>();
		stack.push(n);

		while (!stack.isEmpty()) {
			TreeNode current = stack.pop();
			System.out.println(current.val);

			if (current.right != null)
				stack.push(current.right);

			if (current.left != null)
				stack.push(current.left);
		}
	}

}
