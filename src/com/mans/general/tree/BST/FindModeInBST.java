package com.mans.general.tree.BST;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mans.ds.TreeNode;

public class FindModeInBST {

	public static void main(String[] args) {
		/*TreeNode root = new TreeNode(1);
		root.setRight(new TreeNode(2));*/
		//root.getRight().setRight(new TreeNode(2));
		
		TreeNode root = new TreeNode(2);
		root.right = new TreeNode(3);
		root.right.right = new TreeNode(4);
		root.right.right.right = new TreeNode(5);
		root.right.right.right.right = new TreeNode(6);
		Arrays.stream(findMode(root)).forEach(System.out::println);
	}
	
	public static int[] findMode(TreeNode root) {
        Map<Integer, Integer> countMap = new HashMap<>();
        findMode(root, countMap);
        
        
        int max = Integer.MIN_VALUE;
        List<Integer> mode = new ArrayList<>();
        
        for(Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
            if(entry.getValue() == max) {
                mode.add(entry.getKey());
            }
            else if(entry.getValue() > max) {
            	mode.clear();
            	mode.add(entry.getKey());
            	max = entry.getValue().intValue();
            }
        }
        
        return mode.stream().mapToInt(i -> i).toArray();
    }
    
    public static void findMode(TreeNode root, Map<Integer, Integer> countMap) {
        if(root == null)
            return;
        
        Integer cur = new Integer(root.val);
        
        if(!countMap.containsKey(cur)) {
        	countMap.put(cur, 1);
        }
        else {
        	countMap.put(cur, countMap.get(cur) + 1);
        }
        
        findMode(root.left, countMap);
        findMode(root.right, countMap);
    }

}
