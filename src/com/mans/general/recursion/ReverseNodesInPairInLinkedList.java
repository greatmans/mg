package com.mans.general.recursion;

import com.mans.ds.ListNode;

public class ReverseNodesInPairInLinkedList {

	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		ListNode n = head;
		n.setNext(new ListNode(2));
		n = n.getNext();
		n.setNext(new ListNode(3));
		n = n.getNext();
		n.setNext(new ListNode(4));
		n = n.getNext();
		n.setNext(new ListNode(5));
		n = n.getNext();
		n.setNext(new ListNode(6));

		System.out.println("Before swap: ");
		printList(head);
		System.out.println("\nAfter swap: ");
		printList(swap(head));
	}

	private static void printList(ListNode result) {
		while (result != null) {
			System.out.print(result.getData());
			if (result.getNext() != null)
				System.out.print(" -> ");
			
			result = result.getNext();
		}

	}

	public static ListNode swap(ListNode head) {
		if (head == null || head.getNext() == null)
			return head;

		//Approach 1
		/*ListNode n = head;
		int temp = n.getData();
		n.setData(n.getNext().getData());
		n.getNext().setData(temp);
		swap(n.getNext().getNext());
		*/
		
		//Approach 2
		/*ListNode nextNode = n.getNext();
		ListNode nextOfNextNode = nextNode.getNext();
		n.getNext().setNext(n);
		n = null;
		nextNode.getNext().setNext(swap(nextOfNextNode));
		return nextNode;*/
		
		//Approach 3
		ListNode n = head.getNext();
		ListNode nextSwapped = swap(head.getNext().getNext());
		head.setNext(nextSwapped);
		n.setNext(head);
		head = n;
		
		return head;
	}

}
