package com.mans.general.recursion;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.mans.ds.TreeNode;
import com.mans.general.tree.BFS_DFS_Iterative;
import com.mans.util.DSUtil;

public class InvertAlternateLevelPerfectBinaryTree {

	public static void main(String[] args) {
		int[] arr = DSUtil.generateNumbersForHeight(4);
		TreeNode root = DSUtil.parseArrayToTree(arr);
		//invertAlternate(root);
		invertBinaryTreeDFSRecursive(root);
		BFS_DFS_Iterative.BFS_Iterative(root);
	}
	
	public static void invertBinaryTreeDFSRecursive(TreeNode root) {
		invertBinaryTreeDFSRecursive(root.left, root.right, true);
	}

	public static void invertBinaryTreeDFSRecursive(TreeNode first, TreeNode second, boolean level)
	{
		if (first == null || second == null) {
			return;
		}

		if (level) {
			int temp = first.val;
			first.val = second.val;
			second.val = temp;
		}

		invertBinaryTreeDFSRecursive(first.left, second.right, !level);
		invertBinaryTreeDFSRecursive(first.right, second.left, !level);
	}
	
	public static void invertAlternate(TreeNode root) {
		Queue<TreeNode> queue = new LinkedList<>();
		queue.offer(root);
		boolean invert = true;

		while (!queue.isEmpty()) {
			List<TreeNode> parents = new ArrayList<>();
			LinkedList<Integer> dataStack = new LinkedList<>();

			while (!queue.isEmpty()) {
				TreeNode current = queue.poll();
				parents.add(current);

				if (invert) {
					if (current.left != null)
						dataStack.push(current.left.val);

					if (current.right != null)
						dataStack.push(current.right.val);
				}
			}

			if (invert) {
				int i = 0;
				while (!dataStack.isEmpty()) {
					int leftData = dataStack.pop();
					int rightData = dataStack.pop();

					parents.get(i).left.val = leftData;
					parents.get(i).right.val = rightData;
					i++;
				}
			}

			for (int j = 0; j < parents.size(); j++) {
				if (parents.get(j).left != null)
					queue.offer(parents.get(j).left);

				if (parents.get(j).right != null)
					queue.offer(parents.get(j).right);
			}

			invert = !invert;
		}
	}

}
