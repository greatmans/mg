package com.mans.general.recursion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PowerSet {

	public static void main(String[] args) {
		String[] inputs = new String[] { "", "ab", "abc","abcd" };
		Arrays.stream(inputs)
		.forEach(input -> {
			System.out.println("\n[" + input + "]\n ### start ####");
			List<String> powerSet = generatePowerSet(input);
			powerSet.forEach(System.out::println);
			System.out.println("### end ###");
		});
	}
	
	public static List<String> generatePowerSet(String input) {
		if(input == null)
			return null;
		
		List<String> result = new ArrayList<>();
		String output = "";
		generatePowerSet(input, output, result);
		//sort the list to get result in lexicographical order
		Collections.sort(result);
		
		return result;
	}

	private static void generatePowerSet(String input, String output, List<String> result) {
		if(input.length() == 0) {
			result.add(output);
			return;
		}
		
		String op1 = output;
		String op2 = output + input.charAt(0);
		
		generatePowerSet(input.substring(1), op1, result);
		generatePowerSet(input.substring(1), op2, result);
	}

}
