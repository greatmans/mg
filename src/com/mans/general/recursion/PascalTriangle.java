package com.mans.general.recursion;

import java.util.ArrayList;
import java.util.List;

public class PascalTriangle {

	public static void main(String[] args) {
		getRow(3).forEach(n -> System.out.print(n + " "));
	}
	
	public static List<Integer> getRow(int n) {
		List<Integer> l = new ArrayList<>();
		if(n == 0) {
			l.add(1);
			return l;
		}
		
		if(n == 1) {
			l.add(1);
			l.add(1);
			return l;
		}
		
		List<Integer> prev = getRow(n - 1);
		
		List<Integer> cur = new ArrayList<>();
		cur.add(1);
		for(int i = 0; i < prev.size() - 1; i++) {
			cur.add(prev.get(i) + prev.get(i + 1));
		}
		cur.add(1);
		
		return cur;
	}

}
