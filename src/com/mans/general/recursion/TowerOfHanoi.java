package com.mans.general.recursion;

public class TowerOfHanoi {

	static int step = 1;
	public static void main(String[] args) {
		solve(1, 3, 2, 3);

	}
	
	private static void solve(int s, int d, int h, int disks) {
		if(disks == 1) {
			System.out.println("Step: " + (step++) + ": Move disk " + disks + " from " + s + " to " + d);
			return;
		}
		
		solve(s, h, d, disks - 1);
		System.out.println("Step: " + (step++) + ": Move disk " + disks + " from " + s + " to " + d);
		solve(h, d, s, disks - 1);
	}

}
