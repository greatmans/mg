package com.mans.general.recursion;

import java.util.LinkedList;
import java.util.Queue;

import com.mans.ds.TreeNode;
import com.mans.util.DSUtil;

public class BFS_DFS_Recursion {

	public static void main(String[] args) {
		TreeNode root = DSUtil.parseArrayToTree(new int[] { 1, 2, 3, 4, 5, 6, 7 });
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		
		BFS_Recursive(root, queue);
		System.out.println("BFS recursive Completed");
		
		System.out.println("===================================");
		
		
		/*
		 * In-order -> LDR
		 * Pre-order -> DLR
		 * Post-order -> LRD
		 */
		DFS_Recursive(root);
		System.out.println("DFS Post order recursive Completed");
	}

	public static void BFS_Recursive(TreeNode n, Queue<TreeNode> queue) {
		if (n == null)
			return;

		while (!queue.isEmpty()) {
			TreeNode current = queue.poll();
			System.out.println(current.val);

			if (current.left != null)
				queue.offer(current.left);

			if (current.right != null)
				queue.offer(current.right);

			BFS_Recursive(current.left, queue);
			BFS_Recursive(current.right, queue);
		}
	}
	
	public static void DFS_Recursive(TreeNode n) {
		if(n == null)
			return;
		
		DFS_Recursive(n.left);
		DFS_Recursive(n.right);
		System.out.println(n.val);
	}
}
