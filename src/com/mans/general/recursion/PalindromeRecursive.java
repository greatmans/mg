package com.mans.general.recursion;

import java.util.Arrays;

public class PalindromeRecursive {

	public static void main(String[] args) {
		String[] inputs = new String[] { null, "", " ", "a", "ab", "aba", "abba", "Aa", "strrst", "strrts", "sttst" };
		Arrays.stream(inputs).forEach(input -> System.out.println("[" + input + "]" + " -> " + isPalindrome(input)));
	}

	public static boolean isPalindrome(String input) {
		if(input == null || input.trim().length() == 0)
			return true;
		
		return isPalindrome(input, 0, input.length() - 1);
	}
	
	private static boolean isPalindrome(String input, int low, int high) {
		if((low == high || low == high - 1) && input.charAt(low) == input.charAt(high))
			return true;
		
		if(input.charAt(low) == input.charAt(high)) {
			return isPalindrome(input, low + 1, high - 1);
		}
		
		return false;
	}

}
