package com.mans.general.recursion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ClimbStairs {

	public static void main(String[] args) {
		System.out.println(climbStairs(5));
	}
	
	public static int climbStairs(int n) {
		if(n == 0)
			return 0;
		
        int[] cache = new int[n + 1];
        Arrays.fill(cache, -1);
        int count =  helper(n, new AtomicInteger(0), cache).get();
        return count;
    }

	public static AtomicInteger helper(int ip, AtomicInteger count, int[] cache) {
		if (ip == 0) {
			count.incrementAndGet();
			return count;
		}

		if (cache[ip - 1] != -1) {
			count.addAndGet(cache[ip - 1]);
		} else {
			helper(ip - 1, count, cache);
			cache[ip - 1] = count.get(); 
		}

		if (ip > 1) {
			if (cache[ip - 2] != -1) {
				count.addAndGet(cache[ip - 2]);
			} else {
				helper(ip - 2, count, cache);
				cache[ip - 2] = count.get(); 
			}
		}

		return count;
	}
}
