package com.mans.leetcode.tree.BST;

import com.mans.ds.TreeNode;
import com.mans.util.DSUtil;

public class BSTInsertion {

	public static void main(String[] args) {
		/*TreeNode root = DSUtil.parseArrayToTree(new Integer[] { 4, 2, 7, 1, 3 });
		BFS_DFS_Iterative.BFS_Iterative(insertIntoBST(root, 5));*/
		
		TreeNode root = DSUtil.parseArrayToTree(new Integer[] { 50, 38, 73, 18, 45, 60 });
		DSUtil.printTreeWithNullChildren(insertIntoBST(root, 40));
		System.out.println("hi");
	}

	public static TreeNode insertIntoBST(TreeNode root, int val) {
		if(root == null) {
			root = new TreeNode(val);
			return root;
		}
		
		if(root.left == null && val <= root.val) {
			root.left = new TreeNode(val);
			return root;
		}
		else if(root.right == null && val > root.val) {
			root.right = new TreeNode(val);
			return root;
		}
		
		if(val <= root.val)
			insertIntoBST(root.left, val);
		else
			insertIntoBST(root.right, val);
		
		return root;
	}
}
