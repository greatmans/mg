package com.mans.leetcode.tree.BST;

import com.mans.ds.TreeNode;
import com.mans.general.tree.BFS_DFS_Iterative;
import com.mans.util.DSUtil;

public class ConvertBSTToGreaterTree {

	public static void main(String[] args) {
		TreeNode root = DSUtil.parseArrayToTree(new Integer[] { 4, 1, 6, 0, 2, 5, 7, null, null, null, 3, null, null, null, 8 });
		//TreeNode root = DSUtil.parseArrayToTree(new Integer[] { 3,2,4,1 });
		bstToGst(root);
		BFS_DFS_Iterative.BFS_Iterative(root);
	}

	public static TreeNode bstToGst(TreeNode root) {
		if (root == null || (root.left == null && root.right == null))
			return root;

		bstToGst(root, 0);
		return root;
	}

	public static void bstToGst(TreeNode root, int parentValue) {
		if (root == null)
			return;

		if (root.right != null)
			bstToGst(root.right, parentValue);

		int greaterSum = (root.right == null ? 0 : leftMostChild(root.right).val);
		root.val = root.val + greaterSum;

		if ((root.left == null && root.right == null) || (root.left != null && root.right == null)) 
			root.val = root.val + parentValue;

		if (root.left != null)
			bstToGst(root.left, root.val);
	}

	private static TreeNode leftMostChild(TreeNode root) {
		if (root.left == null)
			return root;

		return leftMostChild(root.left);
	}
	
}


//Leetcode Solution
//No need to keep track of greatestvalue from right. A rollingsum (currsum) variable to maintain it.
class Solution {
    
    int currSum =0;
    public TreeNode bstToGst(TreeNode root) {
        
        if(root == null)
            return root;
        
        bstToGst(root.right);
        root.val = root.val + currSum;
        currSum = root.val;
        bstToGst(root.left);
        return root;
    }
}
